"""
File: conanfile.py
Author: fisherprime
Email: N/A
Gitlab: https://gitlab.com/fisherprime
Description: Conan install script
"""

from multiprocessing import cpu_count

from conans import CMake, ConanFile, tools


class CXXHeadersConan(ConanFile):
    #  version = "1.x"
    description = "CXX util library"
    license = "MIT"
    name = "CXXHeaders"
    url = "https://gitlab.com/fisherprime/cxxheaders"

    build_policy = "missing"  # always
    generators = "cmake"
    #  requires = ("flatbuffers/[~=1.11.0]@google/stable",
                #  "doctest/2.3.1@bincrafters/stable")
    requires = ("flatbuffers/[~=2.0.5]",
                "doctest/2.4.8")

    settings = "os", "compiler", "build_type", "arch"

    _build_type = "Debug"  # Release
    _cmake_generator = ["Ninja", "Unix Makefiles"]

    def _configure_cmake(self):
        cmake = CMake(
            self, generator=self._cmake_generator[0], build_type=self._build_type)
        #  cmake.definitions["USE_FLATCC"] = 1

        cmake.configure()

        return cmake

    def _test(self, cmake):
        if self._build_type == "Debug":
            cmake.test()

    def build(self):
        cmake = self._configure_cmake()

        cmake.build()
        self._test(cmake)

        return cmake

    def package(self):
        cmake = self._configure_cmake()

        cmake.install()

    def package_info(self):
        self.cpp_info.libs = ["lib" + self.name + ".a"]
