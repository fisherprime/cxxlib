<a name=""></a>
##  (2019-09-18)


#### Bug Fixes

* **CMakeLists.txt:**  Clarify c_headers is a lib ([e0373d66](e0373d66))

#### Performance

* **util.cpp:**  Remove unnecessary cout ([0f28625f](0f28625f))

#### Features

* **CHANGELOG.md:**  Add CHANGELOG.md ([2d1d033d](2d1d033d))
* **conanfile.py:**
  *  Add built type check for tests ([213ece82](213ece82))
  *  Add Makefile generator ([b63395d1](b63395d1))
* **tests/Util.cpp:**  Add util functions test ([0aba450b](0aba450b))
