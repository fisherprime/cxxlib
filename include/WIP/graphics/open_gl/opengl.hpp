/**
 * @file opengl.hpp
 * @brief C++ include file, OpenGL 3|4 handling definitions
 * @author fisherprime
 * @version 1.0.1
 * @date 2019-09-18
 *
 * SPDX-License-Identifier: MIT
 */

#ifndef _OPENGL_HPP
#define _OPENGL_HPP

// Necessary headers
// ////////////////////////////////
// OpenGL
// Always include glew.h before gl.h and glfw3.h, since it's a bit magic.
#include <GL/glew.h>

#include <GLFW/glfw3.h> // Handles the window and the keyboard
#include <glm/glm.hpp> // Library for 3D mathematics
#include <glm/gtc/matrix_transform.hpp>

// Shader handling
#include "opengl_shaders.hpp"

// Image file loading
#include "io/load_bmp.h"
// ////////////////////////////////

// Return statuses 100-200 C++
// ////////////////////////////////
#define ERR_GLFW_INITIALIZE 151 // GLFW initialization failure
#define ERR_OPENGL_WINDOW_OPEN 152 // Error opening OpenGl window
#define ERR_GLEW_INITIALIZE 151 // GLEW initialization failure
// ////////////////////////////////

// Definitions
// ////////////////////////////////
#define DEFAULT_ANTIALIAS 8
#define OPENGL_VERSION_MAJOR 3
#define OPENGL_VERSION_MINOR 3
#define DEFAULT_WINDOW_WIDTH 1024
#define DEFAULT_WINDOW_HEIGHT 768
// ////////////////////////////////

// Typedefs
// ////////////////////////////////
// ////////////////////////////////

// Macros
// ////////////////////////////////
// ////////////////////////////////

// Global variables
// ////////////////////////////////
// ////////////////////////////////

// Function prototypes: OpenGl operations
// ////////////////////////////////
// ////////////////////////////////

// Inline functions
// ////////////////////////////////
// ////////////////////////////////

// Extern functions
// ////////////////////////////////
// ////////////////////////////////

#endif // _OPENGL_HPP
